import random

questions = {
    "strong": "Do ye like yer drinks strong?",
    "salty": "Do ye like it with a salty tang?",
    "bitter": "Are ye a lubber who likes it bitter?",
    "sweet": "Would ye like a bit of sweetness with yer poison?",
    "fruity": "Are ye one for a fruity finish?"
}

ingredients = {
    "strong": ["glug of rum", "slug of whisky", "splash of gin"],
    "salty": ["olive on a stick", "salt-dusted rim", "rasher of bacon"],
    "bitter": ["shake of bitters", "splash of tonic", "twist of lemon peel"],
    "sweet": ["sugar cube", "spoonful of honey", "spash of cola"],
    "fruity": ["slice of orange", "dash of cassis", "cherry on top"]
}

stock = {}

def ask_client():
    result = {}
    for drink_type, question in questions.iteritems():
        print question + ": "
        client_input = raw_input("(enter Yes or No) ")
        result[drink_type] = (client_input == "Yes")
    return result

def make_drink(preferences):
    result = []
    for drink_type, choice in preferences.iteritems():
        if choice:
            ingredient = random.choice(ingredients[drink_type])
            if stock[ingredient] == 0:
                restock()
            stock[ingredient] -= 1
            result.append(ingredient)
    return result

def drink_name():
    nouns = ["Chinchilla", "Banana", "Sea-Dog", "Mermaid", "Rum"]
    adjectives = ["Fluffy", "Woody", "Nauseous", "Salty", "Old", "Land Loving"]
    name = random.choice(adjectives) + " " + random.choice(nouns)
    return name

def ask_customer_info():
    print "What's yer name?"
    name = raw_input()
    return name

def restock():
    print "Restocking..."
    for key, ingredients_list in ingredients.iteritems():
        for ingredient in ingredients_list:
            stock[ingredient] = 5

def main():
    restock()
    customers = {}
    while True:
        name = ask_customer_info()
        if customers.has_key(name):
            drink_preferences = customers[name]['drink_preferences']
            final_drink_ingredients = make_drink(drink_preferences)
            print final_drink_ingredients
            print customers[name]['drink_name']
        else:
            customers[name] = {}
            drink_preferences = ask_client()
            final_drink_ingredients = make_drink(drink_preferences)
            drink = drink_name()
            print final_drink_ingredients
            print drink
            customers[name]["drink_preferences"] = drink_preferences
            customers[name]["drink_name"] = drink
        print "Do you want another drink?"
        answer = raw_input("Enter Yes or Y if yes: ")
        if answer not in ["Yes", "y", "Y"]:
            break
    print "You know nothing, Jon Snow."

if __name__ == '__main__':
    main()